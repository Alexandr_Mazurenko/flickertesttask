package com.alehandro.flickrtesttask.Presenters;

import com.alehandro.flickrtesttask.DI.MyApplication;
import com.alehandro.flickrtesttask.Interfaces.IView;
import com.alehandro.flickrtesttask.Interfaces.IViewPresenter;
import com.alehandro.flickrtesttask.Model.Model;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

/**
 * Created by Alehandro on 23.01.2017.
 */

public class DetailFragmentPresenter implements IViewPresenter{
    private WeakReference<IView> mIViewWeakReference;
    @Inject
    protected Model mModel;
    @Inject
    protected Picasso mPicasso;

    public DetailFragmentPresenter(){
        MyApplication.getAppComponent().inject(this);
    }

    @Override
    public void setView(IView iView) {
        mIViewWeakReference = new WeakReference<>(iView);
    }

    public Picasso getPicasso() {
        return mPicasso;
    }

    public String getImageUrl(){
        return mModel.getPhotoList().get(mModel.getChosenPosition()).getPhotoUrl();
    }

    public String getImageTitle(){
        return mModel.getPhotoList().get(mModel.getChosenPosition()).getTitle();
    }



}
