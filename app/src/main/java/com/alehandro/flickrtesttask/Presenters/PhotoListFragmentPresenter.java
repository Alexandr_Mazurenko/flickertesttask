package com.alehandro.flickrtesttask.Presenters;

import android.content.res.Configuration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.alehandro.flickrtesttask.Constants.AppConstants;
import com.alehandro.flickrtesttask.DI.MyApplication;
import com.alehandro.flickrtesttask.Interfaces.FlickerRetrofitInterface;
import com.alehandro.flickrtesttask.Interfaces.IView;
import com.alehandro.flickrtesttask.Interfaces.IViewPresenter;
import com.alehandro.flickrtesttask.Model.Model;
import com.alehandro.flickrtesttask.POJO.Photo;
import com.alehandro.flickrtesttask.POJO.RecentPhotos;
import com.alehandro.flickrtesttask.R;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Alehandro on 23.01.2017.
 */

public class PhotoListFragmentPresenter implements IViewPresenter {
    private IView mIVew;
    @Inject
    protected Model mModel;
    @Inject
    protected FlickerRetrofitInterface mFlickerService;
    private WeakReference<IView> mIViewWeakReference;

    public PhotoListFragmentPresenter(){
        MyApplication.getAppComponent().inject(this);
    }

    @Override
    public void setView(IView iView) {
        mIViewWeakReference = new WeakReference<>(iView);
        mIVew = mIViewWeakReference.get();

    }

    public RecyclerView.LayoutManager getLayoutManager (){
        if(mModel.getCurrentLayout().equals(AppConstants.LINEAR_LAYOUT)) {
            return new LinearLayoutManager(mModel.getActivityContext());
        }else {
            return new GridLayoutManager(mModel.getActivityContext(),getRowNumbers());
        }
    }

    private int getRowNumbers(){
        if(mModel.getActivityContext().getResources().getConfiguration()
                .orientation == Configuration.ORIENTATION_PORTRAIT){
            return mModel.getActivityContext().getResources().getInteger(R.integer.rows_vertical);
        }else {
            return mModel.getActivityContext().getResources().getInteger(R.integer.rows_horizontal);
        }
    }

    public void fillPhotoList(){
        if(mModel.getPhotoList().size()>0){
            Log.i(AppConstants.LOG_TAG, "getFromCollection");
            mIViewWeakReference.get().updateUI();
        }else fetchDataFromFlickr();
    }

    public void fetchDataFromFlickr(){
        Log.i(AppConstants.LOG_TAG,"fetchDataFromFlickr");
        Observable<RecentPhotos> photoList = mFlickerService.getRecentPhotos(AppConstants.API_KEY,
                AppConstants.PHOTOS_PER_PAGE,
                AppConstants.PHOTOS_PAGES,
                AppConstants.REQUEST_FORMAT,
                AppConstants.NO_JSON_CALLBACK);
        photoList.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RecentPhotos>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.i(AppConstants.LOG_TAG, "OnSubscribe");
                    }

                    @Override
                    public void onNext(RecentPhotos recentPhotos) {
                        //mModel.setPhotoList(recentPhotos.getPhotosInfo().getPhotos());
                        mModel.getPhotoList().addAll(recentPhotos.getPhotosInfo().getPhotos());
                        mIVew.updateUI();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(AppConstants.LOG_TAG, "Fetch error "+e.getLocalizedMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.i(AppConstants.LOG_TAG, "Fetch is finished");
                        for (Photo photo: mModel.getPhotoList() ) {
                            Log.i(AppConstants.LOG_TAG,"title="+photo.getTitle()+
                                    "\n"+"url="+photo.getPhotoUrl()+"\n ==================");
                        }

                    }
                });
    }

    public void setScrollPosition(int position){
        mModel.setListScrollPosition(position);
    }

    public int getScrollPosition(){
        return mModel.getListScrollPosition();
    }
}
