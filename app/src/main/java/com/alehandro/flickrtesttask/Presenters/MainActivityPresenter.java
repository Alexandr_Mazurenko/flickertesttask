package com.alehandro.flickrtesttask.Presenters;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.alehandro.flickrtesttask.Constants.AppConstants;
import com.alehandro.flickrtesttask.DI.MyApplication;
import com.alehandro.flickrtesttask.Interfaces.IView;
import com.alehandro.flickrtesttask.Interfaces.IViewPresenter;
import com.alehandro.flickrtesttask.Model.Model;
import com.alehandro.flickrtesttask.UI.DetailFragment;
import com.alehandro.flickrtesttask.UI.PhotoListFragment;

import java.lang.ref.WeakReference;

import javax.inject.Inject;


/**
 * Created by Alehandro on 23.01.2017.
 */

public class MainActivityPresenter implements IViewPresenter {
    private WeakReference<IView> mIViewWeakReference;
    @Inject
    protected Model mModel;

    public MainActivityPresenter() {
        MyApplication.getAppComponent().inject(this);
    }

    @Override
    public void setView(IView iView) {
        mIViewWeakReference = new WeakReference<>(iView);
    }

    public void setActivityContext(Context context) {
        mModel.setActivityContext(context);
    }

    public Fragment getCurrentFragment() {
        Fragment fragment = new Fragment();
        if (mModel.getCurrentFragment().equals(AppConstants.LIST_FRAGMENT)) {
            fragment = new PhotoListFragment();
        }
        if (mModel.getCurrentFragment().equals(AppConstants.DETAIL_FRAGMENT)
                || mModel.isDualPane()) {
            fragment = new DetailFragment();
        }
        return fragment;
    }

    public void setLayout(int itemId) {
        switch (itemId) {
            case AppConstants.MENU_LIST:
                mModel.setCurrentLayout(AppConstants.LINEAR_LAYOUT);
                break;
            case AppConstants.MENU_GRID:
                mModel.setCurrentLayout(AppConstants.GRID_LAYOUT);
        }
        mIViewWeakReference.get().updateUI();
    }

    private void switchToDetail() {
        mModel.setCurrentFragment(AppConstants.DETAIL_FRAGMENT);
        mModel.setDetailFragment(true);
    }

    public void handleDetail(int position) {
        mModel.setChosenPosition(position);
        if (!mModel.isDualPane()) {
            switchToDetail();
        }
        mIViewWeakReference.get().updateUI();
    }

    public void switchToList() {
        mModel.setCurrentFragment(AppConstants.LIST_FRAGMENT);
        mModel.setDetailFragment(false);
        mIViewWeakReference.get().updateUI();
    }

    public boolean isDetailFragment() {
        return mModel.isDetailFragment();
    }

    public void setDualPane(boolean dualPane) {
        mModel.setDualPane(dualPane);
    }

}




