package com.alehandro.flickrtesttask.Presenters;

import android.content.Context;

import com.alehandro.flickrtesttask.DI.MyApplication;
import com.alehandro.flickrtesttask.Interfaces.IListAdapterPresenter;
import com.alehandro.flickrtesttask.Model.Model;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

/**
 * Created by Alehandro on 28.01.2017.
 */

public class PhotoListAdapterPresenter implements IListAdapterPresenter {
    @Inject
    protected Model mModel;
    @Inject
    protected Picasso mPicasso;

    public PhotoListAdapterPresenter(){
        MyApplication.getAppComponent().inject(this);
    }

    @Override
    public int getItemCount() {
        return mModel.getPhotoList().size();
    }

    public Picasso getPicasso(){
        return mPicasso;
    }

    public Context getActivityContext(){
        return mModel.getActivityContext();
    }

    public String getTitle(int position){
        return mModel.getPhotoList().get(position).getTitle();
    }

    public String getImageURL(int position){
        return mModel.getPhotoList().get(position).getPhotoUrl();
    }
}
