package com.alehandro.flickrtesttask.DI;

import com.alehandro.flickrtesttask.Presenters.DetailFragmentPresenter;
import com.alehandro.flickrtesttask.Presenters.PhotoListAdapterPresenter;
import com.alehandro.flickrtesttask.Presenters.PhotoListFragmentPresenter;
import com.alehandro.flickrtesttask.Presenters.MainActivityPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Alehandro on 24.01.2017.
 */
@Module
public class ViewModule {
    @Provides
    @Singleton
    protected DetailFragmentPresenter getDetailFragmentPresenter(){
        return new DetailFragmentPresenter();
    }

    @Provides
    @Singleton
    protected PhotoListFragmentPresenter getListFragmentPresenter(){
        return new PhotoListFragmentPresenter();
    }

    @Provides
    @Singleton
    protected MainActivityPresenter getMainActivityPresenter(){
        return new MainActivityPresenter();
    }

    @Provides
    @Singleton
    protected PhotoListAdapterPresenter getListAdapterPresenter(){
        return new PhotoListAdapterPresenter();
    }

}
