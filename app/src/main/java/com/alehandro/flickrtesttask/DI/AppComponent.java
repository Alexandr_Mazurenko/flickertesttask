package com.alehandro.flickrtesttask.DI;

import com.alehandro.flickrtesttask.Model.Model;
import com.alehandro.flickrtesttask.Presenters.DetailFragmentPresenter;
import com.alehandro.flickrtesttask.Presenters.PhotoListAdapterPresenter;
import com.alehandro.flickrtesttask.Presenters.PhotoListFragmentPresenter;
import com.alehandro.flickrtesttask.Presenters.MainActivityPresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Alehandro on 25.01.2017.
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    void inject (Model model);
    void inject (DetailFragmentPresenter detailFragmentPresenter);
    void inject (PhotoListFragmentPresenter photoListFragmentPresenter);
    void inject (MainActivityPresenter mainActivityPresenter);
    void inject (PhotoListAdapterPresenter photoListAdapterPresenter);


}
