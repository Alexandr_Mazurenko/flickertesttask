package com.alehandro.flickrtesttask.DI;

import android.app.Application;

/**
 * Created by Alehandro on 25.01.2017.
 */

public class MyApplication extends Application {
    private static AppComponent sAppComponent;
    private static ViewComponent sViewComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sAppComponent = buildAppComponent();
        sViewComponent = buildViewComponent();
    }

    private AppComponent buildAppComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    private ViewComponent buildViewComponent() {
        return DaggerViewComponent.builder()
                .viewModule(new ViewModule())
                .build();
    }

    public static AppComponent getAppComponent(){
        return sAppComponent;
    }

    public static ViewComponent getViewComponent(){
        return sViewComponent;
    }
}
