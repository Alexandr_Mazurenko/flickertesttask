package com.alehandro.flickrtesttask.DI;

import com.alehandro.flickrtesttask.Adapters.PhotoListAdapter;
import com.alehandro.flickrtesttask.UI.DetailFragment;
import com.alehandro.flickrtesttask.UI.PhotoListFragment;
import com.alehandro.flickrtesttask.UI.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Alehandro on 25.01.2017.
 */
@Singleton
@Component(modules = ViewModule.class)
public interface ViewComponent {
    void inject(MainActivity activity);
    void inject (PhotoListFragment photoListFragment);
    void inject (DetailFragment detailFragment);
    void inject (PhotoListAdapter photoListAdapter);
}
