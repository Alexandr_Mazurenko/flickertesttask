package com.alehandro.flickrtesttask.DI;

import android.app.Application;
import android.support.v7.widget.LinearLayoutManager;

import com.alehandro.flickrtesttask.Constants.AppConstants;
import com.alehandro.flickrtesttask.Interfaces.FlickerRetrofitInterface;
import com.alehandro.flickrtesttask.Model.Model;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Alehandro on 24.01.2017.
 */

@Module
public class AppModule {
    private Application mApplication;
    private Retrofit mRetrofit;

    public AppModule(Application application) {
        mApplication = application;
    }


    private Gson getGson(){
        return new GsonBuilder().setLenient().create();
    }

    private OkHttpClient getOkHttpClient(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        return new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();
    }


    private Retrofit getRetrofit(){
        return   new Retrofit.Builder()
                .baseUrl(AppConstants.SERVER_URL)
                .client(getOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .build();

    }

    @Provides
    @Singleton
    protected Application providesApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    protected Picasso getPicasso(Application application){
        return Picasso.with(application);
    }

    @Provides
    @Singleton
    protected Model getModel(){
        return new Model();
    }


    @Provides
    @Singleton
    protected FlickerRetrofitInterface getFlickerRetrofitInterface(){
        mRetrofit = getRetrofit();
        return mRetrofit.create(FlickerRetrofitInterface.class);
    }

    @Provides
    @Singleton
    protected LinearLayoutManager getLinearLayoutManager(Application application){
        return new LinearLayoutManager(application);
        }
    }
