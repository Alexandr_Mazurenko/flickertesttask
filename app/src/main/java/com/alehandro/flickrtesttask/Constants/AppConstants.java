package com.alehandro.flickrtesttask.Constants;

import com.alehandro.flickrtesttask.R;

/**
 * Created by Alehandro on 24.01.2017.
 */

public class AppConstants {
       public static final String API_KEY = "cfa3dd3ffa654b092dcb59415138f385";
       public static final String SERVER_URL = "https://api.flickr.com";
       public static final String REQUEST_FORMAT = "json";
       public static final int PHOTOS_PER_PAGE = 5;
       public static final int PHOTOS_PAGES = 1;
       public static final int NO_JSON_CALLBACK = 1;
       public static final String LOG_TAG = "FlickerLog";
       public static final String LIST_FRAGMENT = "listFragment";
       public static final String DETAIL_FRAGMENT = "detailFragment";
       public static final String PHOTO_SIZE = "m";
       public static final int MENU_LIST = 101;
       public static final int MENU_GRID = 102;
       public static final String LINEAR_LAYOUT  = "linearLayout";
       public static final String GRID_LAYOUT  = "gridLayout";

}
