package com.alehandro.flickrtesttask.Model;

import android.content.Context;

import com.alehandro.flickrtesttask.Constants.AppConstants;
import com.alehandro.flickrtesttask.POJO.Photo;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alehandro on 23.01.2017.
 */

public class Model {
    private List<Photo> mPhotoList;
    private Context mActivityContext;
    private String currentFragment;
    private String mCurrentLayout;
    private int mChosenPosition;
    private int mListScrollPosition;
    private boolean mDetailFragment;
    private boolean mDualPane;

    public Model() {
        currentFragment = AppConstants.LIST_FRAGMENT;
        mPhotoList = new ArrayList<>();
        mCurrentLayout = AppConstants.LINEAR_LAYOUT;
        mChosenPosition = 0;
        mListScrollPosition = 0;
    }

    public List<Photo> getPhotoList() {
        return mPhotoList;
    }

    public void setPhotoList(List<Photo> mPhotoList) {
        this.mPhotoList = mPhotoList;
    }

    public Context getActivityContext() {
        return mActivityContext;
    }

    public void setActivityContext(Context mActivityContext) {
        this.mActivityContext = mActivityContext;
    }

    public String getCurrentFragment() {
        return currentFragment;
    }

    public void setCurrentFragment(String currentFragment) {
        this.currentFragment = currentFragment;
    }

    public String getCurrentLayout() {
        return mCurrentLayout;
    }

    public void setCurrentLayout(String mCurrentLayout) {
        this.mCurrentLayout = mCurrentLayout;
    }

    public int getChosenPosition() {
        return mChosenPosition;
    }

    public void setChosenPosition(int mChosenPosition) {
        this.mChosenPosition = mChosenPosition;
    }

    public boolean isDetailFragment() {
        return mDetailFragment;
    }

    public void setDetailFragment(boolean mDetailFragment) {
        this.mDetailFragment = mDetailFragment;
    }

    public boolean isDualPane() {
        return mDualPane;
    }

    public void setDualPane(boolean mDualPane) {
        this.mDualPane = mDualPane;
    }

    public int getListScrollPosition() {
        return mListScrollPosition;
    }

    public void setListScrollPosition(int mListScrollPosition) {
        this.mListScrollPosition = mListScrollPosition;
    }
}



