package com.alehandro.flickrtesttask.Interfaces;

/**
 * Created by Alehandro on 01.02.2017.
 */
@FunctionalInterface
public interface OnFragmentItemClickListener {
    void OnItemClicked(int position);
}
