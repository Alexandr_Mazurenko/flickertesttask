package com.alehandro.flickrtesttask.Interfaces;

/**
 * Created by Alehandro on 23.01.2017.
 */

public interface IView {
    void initUI();
    void updateUI();
}
