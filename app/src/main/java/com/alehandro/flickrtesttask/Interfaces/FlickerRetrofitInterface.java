package com.alehandro.flickrtesttask.Interfaces;

import com.alehandro.flickrtesttask.POJO.Photo;
import com.alehandro.flickrtesttask.POJO.PhotosList;
import com.alehandro.flickrtesttask.POJO.RecentPhotos;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by Alehandro on 24.01.2017.
 */

public interface FlickerRetrofitInterface {

@GET("/services/rest/?method=flickr.photos.getRecent")
Observable<RecentPhotos> getRecentPhotos(@Query("api_key") String apiKey,
                                         @Query("per_page") int perPage,
                                         @Query("page") int page,
                                         @Query("format") String format,
                                         //a necessary line to avoid error with string at the
                                         // begin of Json response
                                         @Query("nojsoncallback") int noJsonCallback );



}
