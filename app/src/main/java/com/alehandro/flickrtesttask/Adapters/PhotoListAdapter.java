package com.alehandro.flickrtesttask.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.alehandro.flickrtesttask.DI.MyApplication;
import com.alehandro.flickrtesttask.Presenters.PhotoListAdapterPresenter;
import com.alehandro.flickrtesttask.R;
import com.alehandro.flickrtesttask.UI.PhotoListItemViewHolder;

import javax.inject.Inject;

/**
 * Created by Alehandro on 28.01.2017.
 */

public class PhotoListAdapter extends RecyclerView.Adapter<PhotoListItemViewHolder> {
    @Inject
    protected PhotoListAdapterPresenter mPresenter;
    private ViewGroup mViewGroup;

    public PhotoListAdapter(){
        MyApplication.getViewComponent().inject(this);
    }

    @Override
    public PhotoListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mViewGroup = (ViewGroup) LayoutInflater.from(mPresenter.getActivityContext())
                .inflate(R.layout.list_item,parent,false);
        return new PhotoListItemViewHolder(mViewGroup);
    }

    @Override
    public void onBindViewHolder(PhotoListItemViewHolder holder, int position) {
        holder.imageTitle.setText(mPresenter.getTitle(position));
        mPresenter.getPicasso()
                .load(mPresenter.getImageURL(position))
                .into(holder.listImage);
    }

    @Override
    public int getItemCount() {
        return mPresenter.getItemCount();
    }




}
