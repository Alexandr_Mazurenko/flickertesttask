package com.alehandro.flickrtesttask.POJO;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alehandro on 28.01.2017.
 */

public class RecentPhotos {

    @SerializedName("photos")
    private PhotosInfo mPhotosInfo;

    @SerializedName("stat")
    private String mStatus;

    public PhotosInfo getPhotosInfo() {
        return mPhotosInfo;
    }
}
