package com.alehandro.flickrtesttask.POJO;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Alehandro on 27.01.2017.
 */

public class PhotosList {

    @SerializedName("photo")
    private List<Photo> mPhotos;

    public List<Photo> getPhotos() {
        return mPhotos;
    }
}
