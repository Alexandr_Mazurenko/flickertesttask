package com.alehandro.flickrtesttask.POJO;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Alehandro on 28.01.2017.
 */

public class PhotosInfo {

    @SerializedName("photo")
    private List<Photo> mPhotos;
    @SerializedName("page")
    private int mPage;
    @SerializedName("pages")
    private int mPages;
    @SerializedName("perpage")
    private int mPerPage;
    @SerializedName("total")
    private int mTotal;

    public List<Photo> getPhotos() {
        return mPhotos;
    }
}
