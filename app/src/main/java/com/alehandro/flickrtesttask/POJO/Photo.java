package com.alehandro.flickrtesttask.POJO;

import com.alehandro.flickrtesttask.Constants.AppConstants;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Alehandro on 23.01.2017.
 */

public class Photo {
    @SerializedName("title")
    private String mTitle;
    @SerializedName("farm")
    private String mFarmId;
    @SerializedName("server")
    private String mServerId;
    @SerializedName("id")
    private String mPhotoId;
    @SerializedName("secret")
    private String mSecret;
    @SerializedName("photo")
    ArrayList<Photo> photo;

    public String getTitle(){
        return mTitle;
    }

    public String getPhotoUrl(){
        return new StringBuilder()
                .append("https://farm")
                .append(mFarmId)
                .append(".staticflickr.com/")
                .append(mServerId)
                .append("/")
                .append(mPhotoId)
                .append("_")
                .append(mSecret)
                .append("_")
                .append(AppConstants.PHOTO_SIZE)
                .append(".jpg")
                .toString();
    }
}
