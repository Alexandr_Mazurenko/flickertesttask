package com.alehandro.flickrtesttask.UI;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alehandro.flickrtesttask.DI.MyApplication;
import com.alehandro.flickrtesttask.Interfaces.IView;
import com.alehandro.flickrtesttask.Presenters.DetailFragmentPresenter;
import com.alehandro.flickrtesttask.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alehandro on 23.01.2017.
 */

public class DetailFragment extends Fragment implements IView {
    @BindView(R.id.detailImageView)
    protected ImageView mDetailImageView;
    @BindView(R.id.detailImageTitle)
    protected TextView mDetailImageTitle;
    @BindView(R.id.detailPhotoContainer)
    protected RelativeLayout mPhotoContainer;
    @Inject
    protected DetailFragmentPresenter mPresenter;
    private View mView;
    private Palette mPalette;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        MyApplication.getViewComponent().inject(this);
        mPresenter.setView(this);
        mView = inflater.inflate(R.layout.detail_fragment, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        initUI();
        updateUI();
    }

    @Override
    public void initUI() {}

    @Override
    public void updateUI() {
        mPresenter.getPicasso()
                .load(mPresenter.getImageUrl())
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        mDetailImageView.setImageBitmap(bitmap);
                        mPalette = Palette.from(bitmap).generate();
                        Palette.Swatch vibrantBackground = mPalette.getLightMutedSwatch();
                        Palette.Swatch textColor = mPalette.getVibrantSwatch();
                        if(vibrantBackground != null){
                            mPhotoContainer.setBackgroundColor(vibrantBackground.getRgb());
                        }
                        if(textColor != null){
                            mDetailImageTitle.setTextColor(textColor.getRgb());
                        }
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

    }
}
