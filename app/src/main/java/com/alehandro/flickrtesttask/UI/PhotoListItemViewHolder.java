package com.alehandro.flickrtesttask.UI;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alehandro.flickrtesttask.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alehandro on 28.01.2017.
 */

public class PhotoListItemViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.listImageView)
    public ImageView listImage;
    @BindView(R.id.imageTitle)
    public TextView imageTitle;

    public PhotoListItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
