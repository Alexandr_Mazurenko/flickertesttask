package com.alehandro.flickrtesttask.UI;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alehandro.flickrtesttask.Adapters.PhotoListAdapter;
import com.alehandro.flickrtesttask.Constants.AppConstants;
import com.alehandro.flickrtesttask.DI.MyApplication;
import com.alehandro.flickrtesttask.Helper.OnVerticalScrollListener;
import com.alehandro.flickrtesttask.Helper.RecyclerItemClickListener;
import com.alehandro.flickrtesttask.Interfaces.IView;
import com.alehandro.flickrtesttask.Interfaces.OnFragmentItemClickListener;
import com.alehandro.flickrtesttask.Presenters.PhotoListFragmentPresenter;
import com.alehandro.flickrtesttask.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alehandro on 23.01.2017.
 */

public class PhotoListFragment extends Fragment implements IView {
    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;
    @Inject
    protected PhotoListFragmentPresenter mPresenter;
    private PhotoListAdapter mPhotoListAdapter;
    private View mView;
    private OnFragmentItemClickListener mClickListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        MyApplication.getViewComponent().inject(this);
        mPresenter.setView(this);
        mView = inflater.inflate(R.layout.list_fragment,container,false);
        return mView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mClickListener = (MainActivity)activity;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ButterKnife.bind(this,view);
        initUI();
        mPresenter.fillPhotoList();
        mRecyclerView.scrollToPosition(mPresenter.getScrollPosition());
    }

    @Override
    public void initUI() {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mPresenter.getLayoutManager());
        mPhotoListAdapter = new PhotoListAdapter();
        mRecyclerView.setAdapter(mPhotoListAdapter);
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), (view, position) -> {
                    Log.i(AppConstants.LOG_TAG, "position:" + position);
                    mClickListener.OnItemClicked(position);
                    mPresenter.setScrollPosition(position);
                })
        );
        mRecyclerView.addOnScrollListener(new OnVerticalScrollListener() {
            @Override
            public void onScrolledToBottom() {
                mPresenter.fetchDataFromFlickr();
            }
        });
    }

    @Override
    public void updateUI() {
        mPhotoListAdapter.notifyDataSetChanged();
        //mRecyclerView.setScrollY(mPresenter.getScrollPosition());
        Log.i(AppConstants.LOG_TAG, "getScrollPosition():"+mPresenter.getScrollPosition());
    }

    @Override
    public void onStop() {
        //mPresenter.setScrollPosition(mRecyclerView.getScrollY());
        super.onStop();
    }
}
