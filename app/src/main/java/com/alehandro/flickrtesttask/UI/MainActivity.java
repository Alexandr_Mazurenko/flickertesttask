package com.alehandro.flickrtesttask.UI;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.alehandro.flickrtesttask.Constants.AppConstants;
import com.alehandro.flickrtesttask.DI.MyApplication;
import com.alehandro.flickrtesttask.Interfaces.IView;
import com.alehandro.flickrtesttask.Interfaces.OnFragmentItemClickListener;
import com.alehandro.flickrtesttask.Presenters.MainActivityPresenter;
import com.alehandro.flickrtesttask.R;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements IView, OnFragmentItemClickListener {
    @Inject
    protected MainActivityPresenter mPresenter;
    @BindString(R.string.menu_list)
    protected String mMenuList;
    @BindString(R.string.menu_grid)
    protected String mMenuGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MyApplication.getViewComponent().inject(this);
        initUI();
        updateUI();
    }

    @Override
    public void initUI() {
        ButterKnife.bind(this);
        mPresenter.setView(this);
        mPresenter.setActivityContext(this);
        if (findViewById(R.id.listFragment) != null) {
            mPresenter.setDualPane(true);
        } else mPresenter.setDualPane(false);
    }

    @Override
    public void updateUI() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer, mPresenter.getCurrentFragment())
                .commit();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, AppConstants.MENU_LIST, Menu.NONE, mMenuList);
        menu.add(Menu.NONE, AppConstants.MENU_GRID, Menu.NONE, mMenuGrid);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mPresenter.setLayout(item.getItemId());
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void OnItemClicked(int position) {
        mPresenter.handleDetail(position);
    }

    @Override
    public void onBackPressed() {
        if (mPresenter.isDetailFragment()) {
            mPresenter.switchToList();
            Log.i(AppConstants.LOG_TAG, "switchToList");
        } else {
            super.onBackPressed();
        }
    }
}
